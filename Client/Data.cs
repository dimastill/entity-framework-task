﻿using Newtonsoft.Json;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace Client
{
    static class Data
    {
        private static string serverAppPath = "http://localhost:5000/project";

        static HubConnection connection;
        public static void SetConnection()
        {
            connection = new HubConnectionBuilder()
                .WithUrl(serverAppPath)
                .Build();

            connection.StartAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine($"There was an error opening the connection:{task.Exception.GetBaseException()}");
                }
                else
                {
                    Console.WriteLine("Connected");
                }
            }).Wait();

            connection.Closed += async (error) =>
            { 
                await System.Threading.Tasks.Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

            connection.On<string>("GetNotification", message =>
            {
                Console.WriteLine(message);
            });
        }


        public static Dictionary<string, int> GetNumberTasks(int userId)
        {
            return connection.InvokeAsync<Dictionary<string, int>>("GetNumberTasksInProject", userId).Result;
        }

        public static List<DataAccess.Models.Task> GetListTasks(int userId)
        {
            return connection.InvokeAsync<List<DataAccess.Models.Task>>("GetListTasks", userId).Result;
        }

        public static object GetListTasksFinished2019(int userId)
        {
            return connection.InvokeAsync<object>("GetListTasksFinished2019", userId).Result;
        }

        public static IEnumerable<dynamic> GetOldUsers()
        {
            return connection.InvokeAsync<IEnumerable<object>>("GetOldUsers").Result;
        }

        public static IEnumerable<DataAccess.Models.Task> GetSortedUsersList()
        {
            return connection.InvokeAsync<IEnumerable<DataAccess.Models.Task>>("GetSortedUsersList").Result;
        }
        
        public static IEnumerable<dynamic> GetInfoAboutTasks(int userId)
        {
            return connection.InvokeAsync<IEnumerable<object>>("GetInfoAboutLastProject", userId).Result;
        }

        public static IEnumerable<dynamic> GetInfoAboutProject(int projectId)
        {
            return connection.InvokeAsync<dynamic>("GetInfoAboutProject", projectId).Result;
        }

        public static IEnumerable<Team> GetAllTeams()
        {
            return connection.InvokeAsync<ICollection<Team>>("Get").Result;
        }

        public static IEnumerable<User> GetAllUsers()
        {
            return connection.InvokeAsync<ICollection<User>>("GetUsers").Result;
        }

        public static IEnumerable<Project> GetAllProjects()
        {
            return connection.InvokeAsync<ICollection<Project>>("GetProjects").Result;
        }

        public static IEnumerable<DataAccess.Models.Task> GetAllTasks()
        {
            return connection.InvokeAsync<ICollection<DataAccess.Models.Task>>("GetTasks").Result;
        }

        public static Team GetTeam(int id)
        {
            return connection.InvokeAsync<Team>("GetById", id).Result;
        }

        public static User GetUser(int id)
        {
            return connection.InvokeAsync<User>("GetUserById", id).Result;
        }

        public static Project GetProject()
        {
            return connection.InvokeAsync<Project>("GetProjectById").Result;
        }

        public static DataAccess.Models.Task GetTask(int id)
        {
            return connection.InvokeAsync<DataAccess.Models.Task>("GetTaskById", id).Result;
        }

        public static void PostTeam(string method, Team team)
        {
            connection.InvokeAsync(method, team);
        }

        public static void Put(string method, int id, Team team)
        {
            connection.InvokeAsync(method, id, team);
        }

        public static void Delete(string method, int id)
        {
            connection.InvokeAsync(method, id);
        }

        public static void GetLogs()
        {
            connection.InvokeAsync("GetLogs");
        }
    }
}
