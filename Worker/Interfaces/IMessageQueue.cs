﻿using RabbitMQ.Client;
using System;

namespace Worker.Interfaces
{
    public interface IMessageQueue : IDisposable
    {
        IModel Channel { get; }
    }
}
