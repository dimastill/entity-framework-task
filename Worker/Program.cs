﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using Worker.Interfaces;
using Worker.QueueServices;

namespace Worker
{
    class Program
    {
        public static IConfiguration Configuration { get; set; } 
        public static void Main(string[] args)
        {
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile($"{Path.GetFullPath(@"..\..\..\")}\\appsettings.json");

            Configuration = configurationBuilder.Build();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<IConnectionFactory, QueueServices.ConnectionFactory>()
                .AddScoped<IMessageQueue, MessageQueue>()
                .AddScoped<IMessageConsumer, MessageConsumer>()
                .AddScoped<IMessageConsumerScope, MessageConsumerScope>()
                .AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>()
                .AddScoped<IMessageProducer, MessageProducer>()
                .AddScoped<IMessageProducerScope, MessageProducerScope>()
                .AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>()
                .AddSingleton(provider => Configuration)
                .AddScoped<MessageService>()
                .BuildServiceProvider();

            var logger = serviceProvider.GetService<ILoggerFactory>()
                .CreateLogger<Program>();
            var bar = serviceProvider.GetService<MessageService>();
            bar.Run();
        }
    }
}
