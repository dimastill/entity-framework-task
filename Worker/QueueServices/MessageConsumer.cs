﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using Worker.Interfaces;
using Worker.Models;

namespace Worker.QueueServices
{
    public class MessageConsumer : IMessageConsumer
    {
        private readonly MessageConsumerSettings _settings;
        private readonly EventingBasicConsumer _consumer;

        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }

        public MessageConsumer(MessageConsumerSettings settings)
        {
            _settings = settings;

            _consumer = new EventingBasicConsumer(_settings.Channel);
        }

        public void Connect()
        {
            if (_settings.SequentialFetch)
            {
                _settings.Channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            }

            _settings.Channel.BasicConsume(_settings.QueueName, _settings.AutoAcknowledge, _consumer);
        }

        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            if(processed)
            {
                _settings.Channel.BasicAck(deliveryTag, multiple: false);
            }
            else
            {
                _settings.Channel.BasicNack(deliveryTag, multiple: false, requeue: true);
            }
        }
    }
}
