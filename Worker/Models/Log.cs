﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worker.Models
{
    public class Log
    {
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
    }
}
