﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;
        private readonly IQueueService queueService;

        public TasksController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor,
            IQueueService queueService)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
            this.queueService = queueService;
        }

        // GET: api/Tasks
        [HttpGet]
        public IEnumerable<DataAccess.Models.Task> Get()
        {
            queueService.PostValue("Get all tasks was triggered");
            return queryProcessor.Process(new GetAllTasksQuery());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}", Name = "GetTask")]
        public DataAccess.Models.Task Get(int id)
        {
            queueService.PostValue($"Get task (id:{id}) was triggered");
            return queryProcessor.Process(new GetTaskQuery { Id = id });
        }

        [Route("ForUser/{userId}")]
        [HttpGet("{userId}", Name = "GetListTasksForUser")]
        public List<DataAccess.Models.Task> GetListTasks(int userId)
        {
            queueService.PostValue($"Get list tasks for user (user id:{userId}) was triggered");
            return queryProcessor.Process(new GetListTasksQuery { UserId = userId });
        }

        [Route("FinishedIn2019/{userId}")]
        [HttpGet("{userId}", Name = "GetFinishedTask")]
        public object GetListTasksFinished2019(int userId)
        {
            queueService.PostValue($"Get task finished in 2019 for user (user id: {userId}) was triggered");
            return queryProcessor.Process(new GetListTasksFinished2019Query { UserId = userId });
        }

        // POST: api/Tasks
        [HttpPost]
        public void Post([FromBody] AddTaskCommand command)
        {
            queueService.PostValue("Post task was triggered");
            commandProcessor.Process(command);
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] UpdateTaskCommand command)
        {
            queueService.PostValue("Put task was triggered");
            command.UpdateId = id;
            commandProcessor.Process(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            queueService.PostValue("Delete task was triggered");
            commandProcessor.Process(new DeleteTaskCommand { Id = id });
        }
    }
}
