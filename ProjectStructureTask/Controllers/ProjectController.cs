﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;
        private readonly IQueueService queueService;
        public ProjectController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, 
            IQueueService queueService)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
            this.queueService = queueService;
        }
        // GET: api/Project
        [HttpGet]
        public IEnumerable<Project> Get()
        {
            queueService.PostValue("Get all projects was triggered");
            return queryProcessor.Process(new GetAllProjectsQuery());
        }

        // GET: api/Project/5
        [HttpGet("{id}", Name = "GetProject")]
        public Project Get(int id)
        {
            queueService.PostValue($"Get project (id:{id}) was triggered");
            return queryProcessor.Process(new GetProjectQuery { ProjectId = id });
        }

        [Route("GetInfoAboutProject/{projectId}")]
        [HttpGet("{projectId}", Name = "GetInfoAboutProject")]
        public object GetInfoAboutProject(int projectId)
        {
            queueService.PostValue($"Get info about project (id:{projectId} was triggered");
            return queryProcessor.Process(new GetInfoAboutProjectQuery { ProjectId = projectId });
        }

        [Route("LastProject/{userId}")]
        [HttpGet("{userId}", Name = "GetInfoAboutLastProject")]
        public IEnumerable<object> GetInfoAboutLastProject(int userId)
        {
            queueService.PostValue("Get last project was triggered");
            return queryProcessor.Process(new GetInfoAboutLastProjectQuery() { UserId = userId });
        }

        // POST: api/Project
        [HttpPost]
        public void Post([FromBody] AddProjectCommand command)
        {
            queueService.PostValue("Post project was triggered");
            commandProcessor.Process(command);
        }

        // PUT: api/Project/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] UpdateProjectCommand command)
        {
            queueService.PostValue("Put project was triggered");
            command.UpdateId = id;
            commandProcessor.Process(command);
        }

        // DELETE: api/Project/0
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            queueService.PostValue($"Delete project (id:{id}) was triggered");
            commandProcessor.Process(new DeleteProjectCommand { ProjectId = id });
        }
    }
}
