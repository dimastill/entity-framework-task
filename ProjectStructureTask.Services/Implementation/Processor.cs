﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Handlers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Implementation
{
    public abstract class Processor
    {
        private Dictionary<Type, Func<object, object>> dictionary;

        public TResult Process<TResult>(ICommand<TResult> command) =>
            (TResult) dictionary[command.GetType()](command);

        public TResult Process<TResult>(IQuery<TResult> command) =>
            (TResult) dictionary[command.GetType()](command);

        protected void RegisterHandlers(IHandlerFactory handlerFactory) =>
            dictionary = handlerFactory.GetHandlers();
    }
}
