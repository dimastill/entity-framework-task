﻿using System;

namespace ProjectStructureTask.Services.DTOs
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
