﻿using DataAccess.Models;
using Microsoft.AspNetCore.SignalR;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = DataAccess.Models.Task;

namespace ProjectStructureTask.Services.Hubs
{
    public class ProjectHub : Hub
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;
        private readonly IQueueService queueService;

        public ProjectHub(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor,
            IQueueService queueService)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
            this.queueService = queueService;
        }

        public IEnumerable<Team> Get()
        {
            queueService.PostValue("Get all teams was triggered");
            return queryProcessor.Process(new GetAllTeamsQuery());
        }

        public IEnumerable<object> GetOldUsers()
        {
            queueService.PostValue("Get old users was triggered");
            return queryProcessor.Process(new GetOldUsersQuery());
        }


        public Team GetById(int id)
        {
            queueService.PostValue("Get team by id was triggered");
            return queryProcessor.Process(new GetTeamQuery { Id = id });
        }

        public bool Post(AddTeamCommand command)
        {
            queueService.PostValue("Post team was triggered");
            return commandProcessor.Process(command);
        }

        public bool PutTeam(int id, UpdateTeamCommand command)
        {
            queueService.PostValue($"Put team (id: {id} was triggered");
            command.UpdateId = id;
            return commandProcessor.Process(command);
        }

        public bool Delete(int id)
        {
            queueService.PostValue($"Delete team (id:{id} was triggered");
            return commandProcessor.Process(new DeleteTeamCommand { Id = id });
        }

        public Dictionary<string, int> GetNumberTasksInProject(int userId)
        {
            queueService.PostValue("Get task in project was triggered");
            return queryProcessor.Process(new GetNumberTasksInProjectQuery { UserId = userId });
        }
        public List<Task> GetListTasks(int userId)
        {
            queueService.PostValue($"Get list tasks for user (user id:{userId}) was triggered");
            return queryProcessor.Process(new GetListTasksQuery { UserId = userId });
        }

        public object GetListTasksFinished2019(int userId)
        {
            queueService.PostValue($"Get task finished in 2019 for user (user id: {userId}) was triggered");
            return queryProcessor.Process(new GetListTasksFinished2019Query { UserId = userId });
        }

        public IEnumerable<Task> GetSortedUsersList()
        {
            queueService.PostValue("Get sorted users was triggered");
            return queryProcessor.Process(new GetSortedUsersListQuery());
        }

        public IEnumerable<object> GetInfoAboutLastProject(int userId)
        {
            queueService.PostValue("Get last project was triggered");
            return queryProcessor.Process(new GetInfoAboutLastProjectQuery() { UserId = userId });
        }
        public object GetInfoAboutProject(int projectId)
        {
            queueService.PostValue($"Get info about project (id:{projectId} was triggered");
            return queryProcessor.Process(new GetInfoAboutProjectQuery { ProjectId = projectId });
        }

        public IEnumerable<User> GetUsers()
        {
            queueService.PostValue("Get all users was triggered");
            return queryProcessor.Process(new GetAllUsersQuery());
        }
        
        public User GetUserById(int id)
        {
            queueService.PostValue($"Get user (id:{id}) was triggered");
            return queryProcessor.Process(new GetUserQuery { Id = id });
        }

        public void PostUser(AddUserCommand command)
        {
            queueService.PostValue("Post user was triggered");
            commandProcessor.Process(command);
        }

        public void PutUser(int id, UpdateUserCommand command)
        {
            queueService.PostValue("Put user was triggered");
            command.UpdateId = id;
            commandProcessor.Process(command);
        }

        public void DeleteUser(int id)
        {
            queueService.PostValue($"Delete user (id:{id}) was triggered");
            commandProcessor.Process(new DeleteUserCommand { Id = id });
        }

        public IEnumerable<Project> GetProjects()
        {
            queueService.PostValue("Get all projects was triggered");
            return queryProcessor.Process(new GetAllProjectsQuery());
        }

        public Project GetProjectById(int id)
        {
            queueService.PostValue($"Get project (id:{id}) was triggered");
            return queryProcessor.Process(new GetProjectQuery { ProjectId = id });
        }

        public void PostProject(AddProjectCommand command)
        {
            queueService.PostValue("Post project was triggered");
            commandProcessor.Process(command);
        }

        public void PutProject(int id, UpdateProjectCommand command)
        {
            queueService.PostValue("Put project was triggered");
            command.UpdateId = id;
            commandProcessor.Process(command);
        }

        public void DeleteProject(int id)
        {
            queueService.PostValue($"Delete project (id:{id}) was triggered");
            commandProcessor.Process(new DeleteProjectCommand { ProjectId = id });
        }

        public IEnumerable<Task> GetTasks()
        {
            queueService.PostValue("Get all tasks was triggered");
            return queryProcessor.Process(new GetAllTasksQuery());
        }
        public Task GetTaskById(int id)
        {
            queueService.PostValue($"Get task (id:{id}) was triggered");
            return queryProcessor.Process(new GetTaskQuery { Id = id });
        }
        
        public void PostTask(AddTaskCommand command)
        {
            queueService.PostValue("Post task was triggered");
            commandProcessor.Process(command);
        }

        public void PutTask(int id, UpdateTaskCommand command)
        {
            queueService.PostValue("Put task was triggered");
            command.UpdateId = id;
            commandProcessor.Process(command);
        }

        public void DeleteTask(int id)
        {
            queueService.PostValue("Delete task was triggered");
            commandProcessor.Process(new DeleteTaskCommand { Id = id });
        }

        public void GetLogs()
        {
            queueService.PostValue("Get logs");
        }
    }
}
