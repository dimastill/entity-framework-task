﻿using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface IQueryHandler<in TQuery, TResult> where TQuery : IQuery<TResult>
    {
        TResult Handle(TQuery query);
    }
}