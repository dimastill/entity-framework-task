﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface IQueueService
    {
        bool PostValue(string value);
    }
}
