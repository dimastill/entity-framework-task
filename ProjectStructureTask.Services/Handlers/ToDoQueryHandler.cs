﻿using AutoMapper;
using DataAccess;
using DataAccess.Models;
using ProjectStructureTask.Services.Queries;
using ProjectStructureTask.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructureTask.Services.Handlers
{
    public class ToDoQueryHandler : IToDoQueryHandler
    {
        private readonly ProjectDbContext dataContext;
        private readonly IMapper mapper;

        public ToDoQueryHandler(ProjectDbContext dataContext, IMapper mapper)
        {
            this.dataContext = dataContext;
            this.mapper = mapper;
        }
        public Dictionary<Type, Func<object, object>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, object>>
            {
                { typeof(GetAllProjectsQuery), t => Handle(t as GetAllProjectsQuery) },
                { typeof(GetAllTasksQuery), t => Handle(t as GetAllTasksQuery) },
                { typeof(GetAllUsersQuery), t => Handle(t as GetAllUsersQuery) },
                { typeof(GetAllTeamsQuery), t => Handle(t as GetAllTeamsQuery) },
                { typeof(GetProjectQuery), t => Handle(t as GetProjectQuery) },
                { typeof(GetTeamQuery), t => Handle(t as GetTeamQuery) },
                { typeof(GetUserQuery), t => Handle(t as GetUserQuery) },
                { typeof(GetTaskQuery), t => Handle(t as GetTaskQuery) },
                { typeof(GetInfoAboutProjectQuery), t => Handle(t as GetInfoAboutProjectQuery) },
                { typeof(GetInfoAboutLastProjectQuery), t => Handle(t as GetInfoAboutLastProjectQuery) },
                { typeof(GetListTasksFinished2019Query), t => Handle(t as GetListTasksFinished2019Query) },
                { typeof(GetListTasksQuery), t => Handle(t as GetListTasksQuery) },
                { typeof(GetNumberTasksInProjectQuery), t => Handle(t as GetNumberTasksInProjectQuery) },
                { typeof(GetOldUsersQuery), t => Handle(t as GetOldUsersQuery) },
                { typeof(GetSortedUsersListQuery), t => Handle(t as GetSortedUsersListQuery) },
                
            };
        }

        private ICollection<Project> Handle(GetAllProjectsQuery query)
        {
            var projects = dataContext.Projects.ToList();
            return projects;
        }

        private ICollection<Task> Handle(GetAllTasksQuery query)
        {
            var tasks = dataContext.Tasks.ToList();
            return tasks;
        }

        private ICollection<User> Handle(GetAllUsersQuery query)
        {
            var users = dataContext.Users.ToList();
            return users;
        }

        private ICollection<Team> Handle(GetAllTeamsQuery query)
        {
            var teams = dataContext.Teams.ToList();
            return teams;
        }

        private Project Handle(GetProjectQuery query)
        {
            var project = dataContext.Projects.Single(findProject => findProject.ProjectId == query.ProjectId);
            return project;
        }

        private Team Handle(GetTeamQuery query)
        {
            var team = dataContext.Teams.Single(findTeam => findTeam.TeamId == query.Id);
            return team;
        }

        private User Handle(GetUserQuery query)
        {
            var user = dataContext.Users.Single(findUser => findUser.UserId == query.Id);
            return user;
        }

        private Task Handle(GetTaskQuery query)
        {
            var task = dataContext.Tasks.Single(findTask => findTask.TaskId == query.Id);
            return task;
        }

        private Dictionary<string, int> Handle(GetNumberTasksInProjectQuery query)
        {
            var resultQuery = dataContext.Projects.GroupJoin(
                dataContext.Tasks.Where(t => t.TaskId == query.UserId),
                p => p, t => t.Project,
                (project, task) => new
                {
                    project,
                    numberTasks = task.Count(t => t.Project.ProjectId == project.ProjectId)
                });

            Dictionary<string, int> result = new Dictionary<string, int>();
            foreach (var item in resultQuery)
            {
                result.Add(item.project.ProjectName, item.numberTasks);
            }

            return result;
        }

        private List<Task> Handle(GetListTasksQuery query)
        {
            var result = dataContext.Tasks.Where(task => task.Perfomer.UserId == query.UserId && task.TaskName.Length < 45);

            return result.ToList();
        }

        private object Handle(GetListTasksFinished2019Query query)
        {
            var result = dataContext.Tasks.Where(task => task.Perfomer.UserId == query.UserId && 
                                                         task.State.Value == "Finished" &&
                                                         task.FinishedAt.Year == 2019)
                                          .Select(task => new { task.TaskId, task.TaskName });

            return result;
        }

        private IEnumerable<object> Handle(GetOldUsersQuery query)
        {
            var oldUsers = dataContext.Teams.GroupJoin(
                dataContext.Users.Where(user => user.Birthday <= DateTime.Now.AddYears(-12) && user.Team != null)
                                 .OrderBy(user => user.RegisteredAt),
                t => t, u => u.Team,
                (team, user) => new
                {
                    team.TeamId,
                    team.TeamName,
                    Users = user.Select(u => u)
                });

            return oldUsers;
        }

        private IEnumerable<Task> Handle(GetSortedUsersListQuery query)
        {
            var sortedUsers = dataContext.Tasks.OrderBy(task => task.TaskName.Length)
                                               .ThenByDescending(task => task.Perfomer.FirstName);

            return sortedUsers;
        }

        private IEnumerable<object> Handle(GetInfoAboutLastProjectQuery query)
        {
            var result = from user in dataContext.Users
                         where user.UserId == query.UserId
                         join project in dataContext.Projects on user.UserId equals project.Author.UserId
                         where project.CreatedAt == dataContext.Projects.Where(p => p.Author.UserId == user.UserId)
                                                            .Max(x => x.CreatedAt)
                         let lastProject = project
                         join task in dataContext.Tasks on project.ProjectId equals task.Project.ProjectId
                         let numberTasksInLastProject = dataContext.Tasks.Count(t => t.Project.ProjectId == lastProject.ProjectId)
                         let numberUnfinishedOrCanceledTasks = dataContext.Tasks.Count(t => 
                            t.Project.ProjectId == lastProject.ProjectId && task.State.Value != "Finished")
                         let longTask = dataContext.Tasks.Where(t => t.Project.ProjectId == lastProject.ProjectId)
                                             .Aggregate((x, y) => (x.FinishedAt - x.CreateAt) >
                                                                  (y.FinishedAt - y.CreateAt) ? x : y)
                         select new
                         {
                             user,
                             lastProject,
                             numberTasksInLastProject,
                             numberUnfinishedOrCanceledTasks,
                             longTask
                         };

            return result;
        }

        private IEnumerable<object> Handle(GetInfoAboutProjectQuery query)
        {
            var result = from project in dataContext.Projects
                         where project.ProjectId == query.ProjectId
                         join task in dataContext.Tasks on project.ProjectId equals task.Project.ProjectId
                         let longDescriptionProject = dataContext.Tasks.Where(t => t.Project.ProjectId == project.ProjectId)
                                                           .Aggregate((x, y) => x.Description.Length >
                                                                                y.Description.Length ? x : y)
                         let shortNameTask = dataContext.Tasks.Where(t => t.Project.ProjectId == project.ProjectId)
                                                           .Aggregate((x, y) => x.TaskName.Length <
                                                                                y.TaskName.Length ? x : y)
                         let tasksInProject = dataContext.Tasks.Where(t => t.Project.ProjectId == project.ProjectId)
                         let numberUsersInProject = tasksInProject.Where(t => t.Project.Description.Length > 25 ||
                                                                              tasksInProject.Count() < 3)
                                                                  .Select(t => t.Perfomer).GroupBy(t => t.UserId).Count()
                         select new
                         {
                             project,
                             longDescriptionProject,
                             shortNameTask,
                             numberUsersInProject
                         };

            return result;
        }
    }
}
