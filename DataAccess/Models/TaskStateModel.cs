﻿namespace DataAccess.Models
{
    public class TaskStateModel
    {
        public long Id { get; set; }
        public string Value { get; set; }
    }
}
