﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class Team
    {
        public int TeamId { get; set; }
        [MaxLength(15)]
        public string TeamName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
