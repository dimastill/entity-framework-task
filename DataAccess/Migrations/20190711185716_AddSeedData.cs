﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, null, new DateTime(1962, 6, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1970, 9, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "DescriptionTest0", "NameTest0", null },
                    { 2, null, new DateTime(1979, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1986, 6, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "DescriptionTest1", "NameTest1", null },
                    { 3, null, new DateTime(2001, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1971, 10, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "DescriptionTest2", "NameTest2", null }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreateAt", "Description", "FinishedAt", "Name", "PerfomerId", "ProjectId", "StateId" },
                values: new object[,]
                {
                    { 1, new DateTime(1987, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "DescriptionTest0", new DateTime(1988, 11, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "NameTest0", null, null, null },
                    { 2, new DateTime(1997, 6, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "DescriptionTest1", new DateTime(2005, 3, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "NameTest1", null, null, null },
                    { 3, new DateTime(1962, 8, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "DescriptionTest2", new DateTime(1969, 3, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "NameTest2", null, null, null },
                    { 4, new DateTime(1972, 9, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "DescriptionTest3", new DateTime(1990, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "NameTest3", null, null, null }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(1968, 7, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "TeamNameTest0" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(1972, 5, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "EmailTest0@email.com", "FirstNameTest0", "LastNameTest0", new DateTime(1998, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 2, new DateTime(2005, 1, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "EmailTest1@email.com", "FirstNameTest1", "LastNameTest1", new DateTime(2007, 4, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
