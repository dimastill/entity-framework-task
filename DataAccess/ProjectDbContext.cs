﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace DataAccess
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base (options)
        { }

        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var teams = new List<Team>
            {
                new Team {TeamId = 1, CreatedAt = DateTime.Parse("27.07.1968"), TeamName = "TeamNameTest0" },
            };

            var users = new List<User>
            {
                new User { UserId = 1, FirstName = "FirstNameTest0", LastName = "LastNameTest0",
                    Email = "EmailTest0@email.com", Birthday = DateTime.Parse("21.05.1972"),
                    RegisteredAt = DateTime.Parse("15.05.1998")
                },
                new User { UserId = 2, FirstName = "FirstNameTest1", LastName = "LastNameTest1",
                    Email = "EmailTest1@email.com", Birthday = DateTime.Parse("12.01.2005"),
                    RegisteredAt = DateTime.Parse("24.04.2007")
                },
            };

            var projects = new List<Project>
            {
                new Project { ProjectId = 1, ProjectName = "NameTest0", CreatedAt = DateTime.Parse("03.06.1962"),
                    Deadline = DateTime.Parse("23.09.1970"), Description = "DescriptionTest0",
                },
                new Project { ProjectId = 2, ProjectName = "NameTest1", CreatedAt = DateTime.Parse("30.12.1979"),
                    Deadline = DateTime.Parse("22.06.1986"), Description = "DescriptionTest1",
                },
                new Project { ProjectId = 3, ProjectName = "NameTest2", CreatedAt = DateTime.Parse("03.07.2001"),
                    Deadline = DateTime.Parse("03.10.1971"), Description = "DescriptionTest2",
                },
            };


            var tasks = new List<Task>
            {
                new Task { TaskId = 1, Description = "DescriptionTest0", TaskName = "NameTest0",
                    CreateAt = DateTime.Parse("25.08.1987"), FinishedAt = DateTime.Parse("05.11.1988"),
                },
                new Task { TaskId = 2, Description = "DescriptionTest1", TaskName = "NameTest1",
                    CreateAt = DateTime.Parse("10.06.1997"), FinishedAt = DateTime.Parse("23.03.2005"),
                },
                new Task { TaskId = 3, Description = "DescriptionTest2", TaskName = "NameTest2",
                    CreateAt = DateTime.Parse("17.08.1962"), FinishedAt = DateTime.Parse("12.03.1969"),
                },
                new Task { TaskId = 4, Description = "DescriptionTest3", TaskName = "NameTest3",
                    CreateAt = DateTime.Parse("24.09.1972"), FinishedAt = DateTime.Parse("15.06.1990"),
                }
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);

            base.OnModelCreating(modelBuilder);
        }
    }
}
